package com.mercadolibre.data.repository

import com.mercadolibre.data.source.local.CategoryLocalDataSource
import com.mercadolibre.data.source.local.LocalDataSource
import com.mercadolibre.data.source.remote.RemoteDataSource
import com.mercadolibre.domain.model.Category
import com.mercadolibre.domain.model.Item
import com.mercadolibre.domain.model.Picture
import com.mercadolibre.domain.resource.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.withContext

class Repository(
    private val localDataSource: LocalDataSource,
    private val categoryLocalDataSource: CategoryLocalDataSource,
    private val remoteDataSource: RemoteDataSource
) {

    suspend fun invoke() {
        withContext(Dispatchers.IO) {
            when (val response = remoteDataSource.getAllCategories()) {
                is Resource.Success -> {
                    categoryLocalDataSource.saveCategories(response.data)
                }
                else -> {}
            }
        }
    }

    suspend fun searchItems(query: String): Resource<Boolean> =

        when (val result = remoteDataSource.searchItems(query)) {
            is Resource.Success -> {
                localDataSource.insertAllItems(result.data)
                Resource.Success(true)
            }
            is Resource.Error -> {
                Resource.Success(false)
            }
            else -> Resource.Error()
        }

    suspend fun getItemPictures(itemId: String): Resource<Boolean> =
        when (val result = remoteDataSource.getItem(itemId)) {
            is Resource.Success -> {
                localDataSource.insertItemDetail(result.data, itemId)
                Resource.Success(true)
            }
            is Resource.Error -> {
                Resource.Success(false)
            }
            else -> Resource.Error()
        }

    fun getAllItems(): Flow<List<Item>> = localDataSource.getAllItems()
    fun getAllFavourites(): Flow<List<Item>> = localDataSource.getAllFavourites()

    fun getAllCategories(): Flow<List<Category>> = categoryLocalDataSource.getAllCategories()

    fun getItem(id: String): Flow<Item> =
        localDataSource.findById(id)

    fun getAllPictures(itemId: String): Flow<List<Picture>> =
        localDataSource.getAllPictures(itemId)

    suspend fun isEmptyPictures(itemId: String): Boolean =
        localDataSource.isEmptyPictures(itemId)


    suspend fun updateItem(item: Item) {
        localDataSource.update(item)
    }

    suspend fun changeFavourite(id: String, favourite: Boolean) =
        localDataSource.changeFavourite(id, favourite)
}
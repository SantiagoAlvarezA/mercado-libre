package com.mercadolibre.data.source.local

import kotlinx.coroutines.flow.Flow
import com.mercadolibre.domain.model.Item
import com.mercadolibre.domain.model.ItemResponse
import com.mercadolibre.domain.model.Picture
import com.mercadolibre.domain.model.Search

interface LocalDataSource {

    suspend fun isEmpty(): Boolean
    suspend fun insertAllItems(search: Search)
    suspend fun insertItemDetail(itemResponse: ItemResponse, itemId: String)
    fun getAllItems(): Flow<List<Item>>
    fun getAllPictures(itemId: String): Flow<List<Picture>>
    suspend fun isEmptyPictures(itemId: String): Boolean
    fun getAllFavourites(): Flow<List<Item>>
    fun findById(id: String): Flow<Item>
    suspend fun update(product: Item)
    suspend fun changeFavourite(id: String, favourite: Boolean)

}
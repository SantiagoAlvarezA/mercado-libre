package com.mercadolibre.data.source.remote


import com.mercadolibre.domain.model.Category
import com.mercadolibre.domain.model.ItemResponse
import com.mercadolibre.domain.model.Search
import com.mercadolibre.domain.resource.Resource

interface RemoteDataSource {

    suspend fun getAllCategories(): Resource<List<Category>>
    suspend fun searchItems(query: String): Resource<Search>
    suspend fun getItem(id: String): Resource<ItemResponse>
}
package com.mercadolibre.data.source.local

import kotlinx.coroutines.flow.Flow
import com.mercadolibre.domain.model.Category

interface CategoryLocalDataSource {

    suspend fun isEmpty(): Boolean
    suspend fun saveCategories(categories: List<Category>)
    fun getAllCategories(): Flow<List<Category>>
    suspend fun findById(id: Int): Category
    suspend fun update(category: Category)
}
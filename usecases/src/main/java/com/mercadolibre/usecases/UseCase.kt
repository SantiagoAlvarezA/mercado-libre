package com.mercadolibre.usecases

import com.mercadolibre.data.repository.Repository
import com.mercadolibre.domain.model.Category
import com.mercadolibre.domain.model.Item
import com.mercadolibre.domain.model.Picture
import com.mercadolibre.domain.model.Search
import com.mercadolibre.domain.resource.Resource
import kotlinx.coroutines.flow.Flow

class UseCase(
    private val repository: Repository
) {

    suspend fun invoke() {
        repository.invoke()
    }

    suspend fun searchItems(query: String): Resource<Boolean> =
        repository.searchItems(query)


    fun getAllItems(): Flow<List<Item>> =
        repository.getAllItems()

    fun getAllFavourites(): Flow<List<Item>> =
        repository.getAllFavourites()

    fun getItem(id: String): Flow<Item> =
        repository.getItem(id)

    fun getAllCategories(): Flow<List<Category>> =
        repository.getAllCategories()

    suspend fun update(item: Item) =
        repository.updateItem(item)

    suspend fun changeFavourite(id: String, favourite: Boolean) =
        repository.changeFavourite(id, favourite)


    suspend fun getItemPictures(itemId: String): Resource<Boolean> {
        return if (repository.isEmptyPictures(itemId)) {
            when (val result = repository.getItemPictures(itemId)) {
                is Resource.Success -> Resource.Success(true)
                else ->
                    Resource.Success(true)
            }
        } else
            Resource.Success(true)
    }

    fun getAllPictures(itemId: String): Flow<List<Picture>> =
        repository.getAllPictures(itemId)


}
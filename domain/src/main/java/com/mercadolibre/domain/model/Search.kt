package com.mercadolibre.domain.model


data class Search(

    var siteId: String? = null,
    var countryDefaultTimeZone: String? = null,
    var query: String? = null,
    var paging: Paging? = Paging(),
    var results: ArrayList<Results> = arrayListOf(),

    )

data class Paging(

    var total: Int? = null,
    var primaryResults: Int? = null,
    var offset: Int? = null,
    var limit: Int? = null

)

data class Ratings(

    var negative: Double? = null,
    var neutral: Double? = null,
    var positive: Double? = null

)

data class Transactions(

    var canceled: Int? = null,
    var period: String? = null,
    var total: Int? = null,
    var ratings: Ratings? = Ratings(),
    var completed: Int? = null

)

data class SellerReputation(

    var powerSellerStatus: String? = null,
    var levelId: String? = null,
    var transactions: Transactions? = Transactions()

)

data class Seller(

    var id: Int? = null,
    var permalink: String? = null,
    var registrationDate: String? = null,
    var carDealer: Boolean? = null,
    var realEstateAgency: Boolean? = null,
    var tags: ArrayList<String> = arrayListOf(),
    var sellerReputation: SellerReputation? = SellerReputation()

)

data class Installments(

    var quantity: Int? = null,
    var amount: Double? = null,
    var rate: Double? = null,
    var currencyId: String? = null

)

data class Address(

    var stateId: String? = null,
    var stateName: String? = null,
    var cityId: String? = null,
    var cityName: String? = null

)

data class Shipping(

    var freeShipping: Boolean? = null,
    var mode: String? = null,
    var tags: ArrayList<String> = arrayListOf(),
    var logisticType: String? = null,
    var storePickUp: Boolean? = null

)

data class Country(

    var id: String? = null,
    var name: String? = null

)

data class State(

    var id: String? = null,
    var name: String? = null

)

data class City(

    var id: String? = null,
    var name: String? = null

)

data class SellerAddress(

    var id: String? = null,
    var comment: String? = null,
    var addressLine: String? = null,
    var zipCode: String? = null,
    var country: Country? = Country(),
    var state: State? = State(),
    var city: City? = City(),
    var latitude: String? = null,
    var longitude: String? = null

)

data class Attributes(

    var id: String? = null,
    var valueName: String? = null,
    var attributeGroupId: String? = null,
    var attributeGroupName: String? = null,
    var valueType: String? = null,
    var name: String? = null,
    var valueId: String? = null,
    var source: String? = null


)

data class Results(

    var id: String,
    var siteId: String? = null,
    var title: String? = null,
    var seller: Seller? = Seller(),
    var price: Double? = null,
    var salePrice: String? = null,
    var currencyId: String? = null,
    var availableQuantity: Int? = null,
    var soldQuantity: Int? = null,
    var buyingMode: String? = null,
    var listingTypeId: String? = null,
    var stopTime: String? = null,
    var condition: String? = null,
    var permalink: String? = null,
    var thumbnail: String? = null,
    var thumbnailId: String? = null,
    var acceptsMercadopago: Boolean,
    var installments: Installments? = Installments(),
    var address: Address? = Address(),
    var promotions: String? = null,
    var shipping: Shipping? = Shipping(),
    var sellerAddress: SellerAddress? = SellerAddress(),
    var attributes: ArrayList<Attributes> = arrayListOf(),
    var originalPrice: String? = null,
    var categoryId: String? = null,
    var officialStoreId: String? = null,
    var domainId: String? = null,
    var catalogProductId: String? = null,
    var tags: ArrayList<String> = arrayListOf(),
    var catalogListing: Boolean,
    var useThumbnailId: Boolean,
    var offerScore: String? = null,
    var offerShare: String? = null,
    var matchScore: String? = null,
    var winnerItemId: String? = null,
    var melicoin: String? = null,
    var discounts: String? = null,
    var inventoryId: String? = null,
    var orderBackend: Int? = null
)


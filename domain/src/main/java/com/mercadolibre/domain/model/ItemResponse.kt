package com.mercadolibre.domain.model

data class ItemResponse(
    val pictures: List<Picture>? = emptyList()
)

data class Picture(
    val id: String,
    val itemId:String,
    val url: String,
    val secureUrl: String?
)
package com.mercadolibre

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import com.mercadolibre.ui.navigation.Navigation
import com.mercadolibre.ui.theme.SantiagoAlvarezMercadoLibreTheme
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            SantiagoAlvarezMercadoLibreTheme {
                Navigation()
            }
        }
    }
}
package com.mercadolibre.data.server.model

data class CategoryResponse(
    val id: String,
    val name: String
)

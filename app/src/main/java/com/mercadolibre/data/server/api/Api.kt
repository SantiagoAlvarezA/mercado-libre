package com.mercadolibre.data.server.api

import com.mercadolibre.data.server.model.ItemResponse
import com.mercadolibre.data.server.model.SearchResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface Api {

    @GET("sites/MLA/categories")
    suspend fun getCategories(): Response<List<Any>>

    @GET("sites/MLA/search")
    suspend fun searchItems(@Query("q") query: String): Response<SearchResponse>


    @GET("/items/{id}")
    suspend fun getItem(@Path("id") id: String): Response<ItemResponse>

}
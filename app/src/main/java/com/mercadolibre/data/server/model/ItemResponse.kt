package com.mercadolibre.data.server.model

import com.google.gson.annotations.SerializedName


data class ItemResponse(
    val pictures: List<Picture>? = emptyList()
)


data class Picture(
    val id: String,
    val url: String,
    @SerializedName("secure_url") val secureUrl: String?
)

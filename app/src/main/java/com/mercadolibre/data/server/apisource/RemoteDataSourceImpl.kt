package com.mercadolibre.data.server.apisource

import com.google.gson.Gson
import com.mercadolibre.data.Resource
import com.mercadolibre.data.mappers.toDomainCategory
import com.mercadolibre.data.server.BaseApiCall
import com.mercadolibre.data.server.api.Api
import com.mercadolibre.data.server.model.CategoryResponse
import com.mercadolibre.data.server.model.ItemResponse as ServerItemResponse
import com.mercadolibre.data.server.model.SearchResponse
import com.mercadolibre.data.source.remote.RemoteDataSource
import com.mercadolibre.domain.model.Category
import com.mercadolibre.domain.model.ItemResponse
import com.mercadolibre.domain.model.Search
import javax.inject.Inject
import com.mercadolibre.domain.resource.Resource as ResourceDomain


class RemoteDataSourceImpl @Inject constructor(
    private val api: Api
) : RemoteDataSource, BaseApiCall() {

    override suspend fun getAllCategories(): ResourceDomain<List<Category>> =
        when (val response = safeApiCall(call = { api.getCategories() })) {
            is Resource.Success -> mapToListCategoryResponse(response.data)
            is Resource.Error -> {
                com.mercadolibre.domain.resource.Resource.Error()
            }
            else -> com.mercadolibre.domain.resource.Resource.Error()
        }

    private fun mapToListCategoryResponse(response: List<Any>): ResourceDomain<List<Category>> {
        val gson = Gson()
        val jsonString: String = gson.toJson(response)
        val data = gson.fromJson(jsonString, Array<CategoryResponse>::class.java).asList().map {
            it.toDomainCategory()
        }
        return ResourceDomain.Success(data)
    }

    override suspend fun searchItems(query: String): ResourceDomain<Search> =
        when (val response = safeApiCall(call = { api.searchItems(query) })) {
            is Resource.Success -> mapToSearchResponse(response.data)
            is Resource.Error -> {
                ResourceDomain.Error()
            }
            else -> ResourceDomain.Error()
        }

    private fun mapToSearchResponse(response: SearchResponse): ResourceDomain<Search> {
        val gson = Gson()
        val jsonString: String = gson.toJson(response)
        return ResourceDomain.Success(gson.fromJson(jsonString, Search::class.java))
    }

    override suspend fun getItem(id: String): ResourceDomain<ItemResponse> =
        when (val response = safeApiCall(call = { api.getItem(id) })) {
            is Resource.Success -> mapToItemResponse(response.data)
            is Resource.Error -> {
                ResourceDomain.Error()
            }
            else -> ResourceDomain.Error()
        }

    private fun mapToItemResponse(response: ServerItemResponse): ResourceDomain<ItemResponse> {
        val gson = Gson()
        val jsonString: String = gson.toJson(response)
        return ResourceDomain.Success(gson.fromJson(jsonString, ItemResponse::class.java))
    }
}

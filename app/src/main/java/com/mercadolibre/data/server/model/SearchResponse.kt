package com.mercadolibre.data.server.model

import com.google.gson.annotations.SerializedName

data class SearchResponse(

    @SerializedName("site_id") var siteId: String? = null,
    @SerializedName("country_default_time_zone") var countryDefaultTimeZone: String? = null,
    @SerializedName("query") var query: String? = null,
    @SerializedName("paging") var paging: Paging? = Paging(),
    @SerializedName("results") var results: ArrayList<Results> = arrayListOf(),

    )

data class Paging(

    @SerializedName("total") var total: Int? = null,
    @SerializedName("primary_results") var primaryResults: Int? = null,
    @SerializedName("offset") var offset: Int? = null,
    @SerializedName("limit") var limit: Int? = null

)


data class Ratings(

    @SerializedName("negative") var negative: Double? = null,
    @SerializedName("neutral") var neutral: Double? = null,
    @SerializedName("positive") var positive: Double? = null

)

data class Transactions(

    @SerializedName("canceled") var canceled: Int? = null,
    @SerializedName("period") var period: String? = null,
    @SerializedName("total") var total: Int? = null,
    @SerializedName("ratings") var ratings: Ratings? = Ratings(),
    @SerializedName("completed") var completed: Int? = null

)

data class SellerReputation(

    @SerializedName("power_seller_status") var powerSellerStatus: String? = null,
    @SerializedName("level_id") var levelId: String? = null,
    @SerializedName("transactions") var transactions: Transactions? = Transactions()

)

data class Seller(

    @SerializedName("id") var id: Int? = null,
    @SerializedName("permalink") var permalink: String? = null,
    @SerializedName("registration_date") var registrationDate: String? = null,
    @SerializedName("car_dealer") var carDealer: Boolean? = null,
    @SerializedName("real_estate_agency") var realEstateAgency: Boolean? = null,
    @SerializedName("tags") var tags: ArrayList<String> = arrayListOf(),
    @SerializedName("seller_reputation") var sellerReputation: SellerReputation? = SellerReputation()

)

data class Installments(

    @SerializedName("quantity") var quantity: Int? = null,
    @SerializedName("amount") var amount: Double? = null,
    @SerializedName("rate") var rate: Double? = null,
    @SerializedName("currency_id") var currencyId: String? = null

)

data class Address(

    @SerializedName("state_id") var stateId: String? = null,
    @SerializedName("state_name") var stateName: String? = null,
    @SerializedName("city_id") var cityId: String? = null,
    @SerializedName("city_name") var cityName: String? = null

)

data class Shipping(

    @SerializedName("free_shipping") var freeShipping: Boolean? = null,
    @SerializedName("mode") var mode: String? = null,
    @SerializedName("tags") var tags: ArrayList<String> = arrayListOf(),
    @SerializedName("logistic_type") var logisticType: String? = null,
    @SerializedName("store_pick_up") var storePickUp: Boolean? = null

)

data class Country(

    @SerializedName("id") var id: String? = null,
    @SerializedName("name") var name: String? = null

)

data class State(

    @SerializedName("id") var id: String? = null,
    @SerializedName("name") var name: String? = null

)

data class City(

    @SerializedName("id") var id: String? = null,
    @SerializedName("name") var name: String? = null

)

data class SellerAddress(

    @SerializedName("id") var id: String? = null,
    @SerializedName("comment") var comment: String? = null,
    @SerializedName("address_line") var addressLine: String? = null,
    @SerializedName("zip_code") var zipCode: String? = null,
    @SerializedName("country") var country: Country? = Country(),
    @SerializedName("state") var state: State? = State(),
    @SerializedName("city") var city: City? = City(),
    @SerializedName("latitude") var latitude: String? = null,
    @SerializedName("longitude") var longitude: String? = null

)

data class Attributes(

    @SerializedName("id") var id: String? = null,
    @SerializedName("value_name") var valueName: String? = null,
    @SerializedName("attribute_group_id") var attributeGroupId: String? = null,
    @SerializedName("attribute_group_name") var attributeGroupName: String? = null,
    @SerializedName("value_type") var valueType: String? = null,
    @SerializedName("name") var name: String? = null,
    @SerializedName("value_id") var valueId: String? = null,
    @SerializedName("source") var source: String? = null


)

data class Results(

    @SerializedName("id") var id: String? = null,
    @SerializedName("site_id") var siteId: String? = null,
    @SerializedName("title") var title: String? = null,
    @SerializedName("seller") var seller: Seller? = Seller(),
    @SerializedName("price") var price: Double? = null,
    //@SerializedName("prices") var prices: Prices? = Prices(),
    @SerializedName("sale_price") var salePrice: String? = null,
    @SerializedName("currency_id") var currencyId: String? = null,
    @SerializedName("available_quantity") var availableQuantity: Int? = null,
    @SerializedName("sold_quantity") var soldQuantity: Int? = null,
    @SerializedName("buying_mode") var buyingMode: String? = null,
    @SerializedName("listing_type_id") var listingTypeId: String? = null,
    @SerializedName("stop_time") var stopTime: String? = null,
    @SerializedName("condition") var condition: String? = null,
    @SerializedName("permalink") var permalink: String? = null,
    @SerializedName("thumbnail") var thumbnail: String? = null,
    @SerializedName("thumbnail_id") var thumbnailId: String? = null,
    @SerializedName("accepts_mercadopago") var acceptsMercadopago: Boolean? = null,
    @SerializedName("installments") var installments: Installments? = Installments(),
    @SerializedName("address") var address: Address? = Address(),
    @SerializedName("promotions") var promotions: String? = null,
    @SerializedName("shipping") var shipping: Shipping? = Shipping(),
    @SerializedName("seller_address") var sellerAddress: SellerAddress? = SellerAddress(),
    @SerializedName("attributes") var attributes: ArrayList<Attributes> = arrayListOf(),
    @SerializedName("original_price") var originalPrice: String? = null,
    @SerializedName("category_id") var categoryId: String? = null,
    @SerializedName("official_store_id") var officialStoreId: String? = null,
    @SerializedName("domain_id") var domainId: String? = null,
    @SerializedName("catalog_product_id") var catalogProductId: String? = null,
    @SerializedName("tags") var tags: ArrayList<String> = arrayListOf(),
    @SerializedName("catalog_listing") var catalogListing: Boolean? = null,
    @SerializedName("use_thumbnail_id") var useThumbnailId: Boolean? = null,
    @SerializedName("offer_score") var offerScore: String? = null,
    @SerializedName("offer_share") var offerShare: String? = null,
    @SerializedName("match_score") var matchScore: String? = null,
    @SerializedName("winner_item_id") var winnerItemId: String? = null,
    @SerializedName("melicoin") var melicoin: String? = null,
    @SerializedName("discounts") var discounts: String? = null,
    @SerializedName("inventory_id") var inventoryId: String? = null,
    @SerializedName("order_backend") var orderBackend: Int? = null

)

/*
data class Conditions(

    @SerializedName("context_restrictions") var contextRestrictions: ArrayList<String> = arrayListOf(),
    @SerializedName("start_time") var startTime: String? = null,
    @SerializedName("end_time") var endTime: String? = null,
    @SerializedName("eligible") var eligible: Boolean? = null

)


data class Metadata(

    @SerializedName("id") var id: String? = null,
)

data class Prices(

    @SerializedName("id") var id: String? = null,
    @SerializedName("type") var type: String? = null,
    @SerializedName("amount") var amount: Double? = null,
    @SerializedName("regular_amount") var regularAmount: String? = null,
    @SerializedName("currency_id") var currencyId: String? = null,
    @SerializedName("last_updated") var lastUpdated: String? = null,
    @SerializedName("conditions") var conditions: Conditions? = Conditions(),
    @SerializedName("exchange_rate_context") var exchangeRateContext: String? = null,
    @SerializedName("metadata") var metadata: Metadata? = Metadata()

)

data class Prices(

    @SerializedName("id") var id: String? = null,
    @SerializedName("prices") var prices: ArrayList<Prices> = arrayListOf(),
    @SerializedName("presentation") var presentation: Presentation? = Presentation(),
    @SerializedName("payment_method_prices") var paymentMethodPrices: ArrayList<String> = arrayListOf(),
    @SerializedName("reference_prices") var referencePrices: ArrayList<ReferencePrices> = arrayListOf(),
    @SerializedName("purchase_discounts") var purchaseDiscounts: ArrayList<String> = arrayListOf()
)

data class Presentation(

    @SerializedName("display_currency") var displayCurrency: String? = null

)


data class ReferencePrices(

    @SerializedName("id") var id: String? = null,
    @SerializedName("type") var type: String? = null,
    @SerializedName("conditions") var conditions: Conditions? = Conditions(),
    @SerializedName("amount") var amount: Double? = null,
    @SerializedName("currency_id") var currencyId: String? = null,
    @SerializedName("exchange_rate_context") var exchangeRateContext: String? = null,
    @SerializedName("tags") var tags: ArrayList<String> = arrayListOf(),
    @SerializedName("last_updated") var lastUpdated: String? = null

)
*/


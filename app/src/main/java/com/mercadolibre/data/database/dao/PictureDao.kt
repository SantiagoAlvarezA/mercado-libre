package com.mercadolibre.data.database.dao

import androidx.room.*
import com.mercadolibre.data.database.model.Category
import com.mercadolibre.data.database.model.Picture
import kotlinx.coroutines.flow.Flow

@Dao
interface PictureDao {

    @Query("SELECT * FROM Picture WHERE itemId = :itemId")
    fun getAllPictures(itemId: String): Flow<List<Picture>>


    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertPictures(categories: List<Picture>)

    @Query(
        """
        DELETE FROM Picture WHERE itemId = :itemId
    """
    )
    suspend fun delete(itemId: String)

    @Query(
        """
        DELETE FROM Picture
    """
    )
    suspend fun delete()

    @Query("SELECT COUNT(id) FROM Picture WHERE itemId = :itemId")
    suspend fun itemsCount(itemId: String): Int
}


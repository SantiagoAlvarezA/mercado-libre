package com.mercadolibre.data.database.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Category(
    @PrimaryKey(autoGenerate = false)
    val id: String,
    val name: String
)

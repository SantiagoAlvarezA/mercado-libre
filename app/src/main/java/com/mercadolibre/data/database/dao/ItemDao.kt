package com.mercadolibre.data.database.dao

import androidx.room.*
import kotlinx.coroutines.flow.Flow
import com.mercadolibre.data.database.model.Item


@Dao
interface ItemDao {

    @Query("SELECT * FROM Item")
    fun getAll(): Flow<List<Item>>

    @Query("SELECT * FROM Item WHERE favourite = 1")
    fun getAllFavourites(): Flow<List<Item>>

    @Query("SELECT * FROM Item WHERE id = :id ")
    fun findById(id: String): Flow<Item?>

    @Query("SELECT COUNT(id) FROM Item")
    fun itemsCount(): Int

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertItems(items: List<Item>)

    @Update
    fun updateItem(item: Item)

    @Query(
        """
        UPDATE 
            Item 
        SET favourite = :favourite 
        WHERE id = :id
    """
    )
    suspend fun changeFavourite(id: String, favourite: Boolean)

    @Query(
        """
        DELETE FROM Item
    """
    )
    suspend fun delete()

    @Query(
        """
        DELETE FROM Item WHERE favourite = 0
    """
    )
    suspend fun deleteNoFavourite()
}
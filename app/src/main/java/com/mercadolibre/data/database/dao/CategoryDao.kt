package com.mercadolibre.data.database.dao

import androidx.room.*
import kotlinx.coroutines.flow.Flow
import com.mercadolibre.data.database.model.Category

@Dao
interface CategoryDao {

    @Query("SELECT * FROM Category")
    fun getAll(): Flow<List<Category>>

    @Query("SELECT * FROM Category WHERE id = :id")
    fun findById(id: Int): Category

    @Query("SELECT COUNT(id) FROM Category")
    fun categoryCount(): Int

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertCategories(categories: List<Category>)

    @Update
    fun updateCategory(category: Category)
}
package com.mercadolibre.data.database.datasource

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import com.mercadolibre.data.source.local.LocalDataSource
import com.mercadolibre.domain.model.Item
import com.mercadolibre.data.database.dao.ItemDao
import com.mercadolibre.data.database.dao.PictureDao
import com.mercadolibre.data.mappers.mapToItemDomain
import com.mercadolibre.data.mappers.toDomainItem
import com.mercadolibre.data.mappers.toRoomItem
import com.mercadolibre.data.mappers.toRoomPicture
import com.mercadolibre.domain.model.ItemResponse
import com.mercadolibre.domain.model.Picture
import com.mercadolibre.domain.model.Search
import javax.inject.Inject

class ItemDataSource @Inject constructor(
    private val itemDao: ItemDao,
    private val pictureDao: PictureDao
) : LocalDataSource {
    override suspend fun isEmpty(): Boolean =
        itemDao.itemsCount() <= 0

    override suspend fun insertAllItems(search: Search) {
        itemDao.deleteNoFavourite()
        itemDao.insertItems(search.results.map { it.mapToItemDomain().toRoomItem() })
    }

    override fun getAllItems(): Flow<List<Item>> =
        flow {
            val items = itemDao.getAll()
            items.collect {
                if (it.isEmpty())
                    emit(emptyList())
                else
                    emit(it.map { _it -> _it.toDomainItem() })
            }
        }

    override fun getAllFavourites(): Flow<List<Item>> =
        flow {
            val items = itemDao.getAllFavourites()
            items.collect {
                if (it.isNotEmpty())
                    emit(it.map { _it -> _it.toDomainItem() })
                else
                    emit(emptyList())
            }
        }


    override fun findById(id: String): Flow<Item> = flow {
        val item = itemDao.findById(id)
        item.collect {
            if (it != null)
                emit(it.toDomainItem())
        }
    }

    override suspend fun update(product: Item) =
        itemDao.updateItem(product.toRoomItem())

    override suspend fun changeFavourite(id: String, favourite: Boolean) =
        itemDao.changeFavourite(id, favourite)

    override suspend fun insertItemDetail(itemResponse: ItemResponse, itemId: String) {
        val list = itemResponse.pictures?.map { it.toRoomPicture(itemId) }
        requireNotNull(list)
        pictureDao.insertPictures(list)
    }

    override fun getAllPictures(itemId: String): Flow<List<Picture>> = flow {
        val pictures = pictureDao.getAllPictures(itemId)
        pictures.collect {
            if (it.isNullOrEmpty())
                emit(emptyList())
            else
                emit(it.map { _it -> _it.toDomainItem() })
        }
    }

    override suspend fun isEmptyPictures(itemId: String): Boolean =
        pictureDao.itemsCount(itemId) <= 0
}
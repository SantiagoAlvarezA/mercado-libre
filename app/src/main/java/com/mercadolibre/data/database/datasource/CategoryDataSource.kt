package com.mercadolibre.data.database.datasource

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import com.mercadolibre.data.source.local.CategoryLocalDataSource
import com.mercadolibre.domain.model.Category
import com.mercadolibre.data.mappers.toDomainCategory
import com.mercadolibre.data.mappers.toRoomCategory
import com.mercadolibre.data.database.dao.CategoryDao
import javax.inject.Inject

class CategoryDataSource @Inject constructor(
    private val categoryDao: CategoryDao
) : CategoryLocalDataSource {

    override suspend fun isEmpty(): Boolean =
        categoryDao.categoryCount() <= 0

    override suspend fun saveCategories(categories: List<Category>) =
        categoryDao.insertCategories(categories.map { it.toRoomCategory() })

    override fun getAllCategories(): Flow<List<Category>> =
        flow {
            val products = categoryDao.getAll()
            products.collect {
                emit(it.map { _it -> _it.toDomainCategory() })
            }
        }

    override suspend fun findById(id: Int): Category =
        categoryDao.findById(id).toDomainCategory()

    override suspend fun update(category: Category) =
        categoryDao.updateCategory(category.toRoomCategory())
}
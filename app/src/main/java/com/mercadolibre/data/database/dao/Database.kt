package com.mercadolibre.data.database.dao

import androidx.room.Database
import androidx.room.RoomDatabase
import com.mercadolibre.data.database.model.Category
import com.mercadolibre.data.database.model.Item
import com.mercadolibre.data.database.model.Picture

@Database(
    entities = [Item::class, Category::class, Picture::class],
    version = 1,
    exportSchema = true,
    autoMigrations = []
)
abstract class Database : RoomDatabase() {

    abstract fun itemDao(): ItemDao
    abstract fun categoryDao(): CategoryDao
    abstract fun pictureDao(): PictureDao
}
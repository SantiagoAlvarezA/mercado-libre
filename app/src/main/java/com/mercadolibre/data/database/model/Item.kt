package com.mercadolibre.data.database.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Item(
    @PrimaryKey(autoGenerate = false)
    val id: String,
    val siteId: String?,
    var title: String?,
    var price: Double?,
    var salePrice: String?,
    var currencyId: String?,
    var availableQuantity: Int?,
    var soldQuantity: Int?,
    var buyingMode: String?,
    var listingTypeId: String?,
    var stopTime: String?,
    var condition: String?,
    var permalink: String?,
    var thumbnail: String?,
    var thumbnailId: String?,
    var acceptsMercadopago: Boolean?,
    var promotions: String?,
    var originalPrice: String?,
    var categoryId: String?,
    var officialStoreId: String?,
    var domainId: String?,
    var catalogProductId: String?,
    var catalogListing: Boolean?,
    var useThumbnailId: Boolean?,
    var offerScore: String?,
    var offerShare: String?,
    var matchScore: String?,
    var winnerItemId: String?,
    var melicoin: String?,
    var discounts: String?,
    var inventoryId: String?,
    var orderBackend: Int?,
    var favourite: Boolean = false
)

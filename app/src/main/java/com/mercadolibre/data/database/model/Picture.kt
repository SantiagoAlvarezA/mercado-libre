package com.mercadolibre.data.database.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

@Entity(
    foreignKeys = [ForeignKey(
        entity = Item::class,
        parentColumns = arrayOf("id"),
        childColumns = arrayOf("itemId"),
        onDelete = ForeignKey.CASCADE
    )]
)
data class Picture(
    @PrimaryKey(autoGenerate = false) val id: String,
    @ColumnInfo(index = true) val itemId: String,
    val url: String,
    val secureUrl: String?
)
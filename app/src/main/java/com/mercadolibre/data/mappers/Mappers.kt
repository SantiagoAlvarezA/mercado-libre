package com.mercadolibre.data.mappers

import com.mercadolibre.domain.model.Category
import com.mercadolibre.domain.model.Item
import com.mercadolibre.data.database.model.Category as DomainCategory
import com.mercadolibre.data.database.model.Item as DomainItem
import com.mercadolibre.data.server.model.CategoryResponse as ServerCategory


fun Item.toRoomItem(): DomainItem = DomainItem(
    id,
    siteId,
    title,
    price,
    salePrice,
    currencyId,
    availableQuantity,
    soldQuantity,
    buyingMode,
    listingTypeId,
    stopTime,
    condition,
    permalink,
    thumbnail,
    thumbnailId,
    acceptsMercadopago ,
    promotions,
    originalPrice,
    categoryId,
    officialStoreId,
    domainId,
    catalogProductId,
    catalogListing ,
    useThumbnailId ,
    offerScore ,
    offerShare,
    matchScore,
    winnerItemId,
    melicoin,
    discounts,
    inventoryId,
    orderBackend,
    favourite
)

fun DomainItem.toDomainItem(): Item = Item(
    id,
    siteId,
    title,
    price,
    salePrice,
    currencyId,
    availableQuantity,
    soldQuantity,
    buyingMode,
    listingTypeId,
    stopTime,
    condition,
    permalink,
    thumbnail,
    thumbnailId,
    acceptsMercadopago,
    promotions,
    originalPrice,
    categoryId,
    officialStoreId,
    domainId,
    catalogProductId,
    catalogListing,
    useThumbnailId,
    offerScore,
    offerShare,
    matchScore,
    winnerItemId,
    melicoin,
    discounts,
    inventoryId,
    orderBackend,
    favourite
)


fun Category.toRoomCategory(): DomainCategory = DomainCategory(
    id, name
)

fun DomainCategory.toDomainCategory(): Category = Category(
    id, name
)

fun ServerCategory.toDomainCategory(): Category = Category(
    id, name
)

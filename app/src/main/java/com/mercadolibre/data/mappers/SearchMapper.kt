package com.mercadolibre.data.mappers

import  com.mercadolibre.domain.model.Item
import com.mercadolibre.domain.model.Results

fun Results.mapToItemDomain(): Item =
    Item(
        id,
        siteId,
        title,
        price,
        salePrice,
        currencyId,
        availableQuantity,
        soldQuantity,
        buyingMode,
        listingTypeId,
        stopTime,
        condition,
        permalink,
        thumbnail,
        thumbnailId,
        acceptsMercadopago,
        promotions,
        originalPrice,
        categoryId,
        officialStoreId,
        domainId,
        catalogProductId,
        catalogListing,
        useThumbnailId,
        offerScore,
        offerShare,
        matchScore,
        winnerItemId,
        melicoin,
        discounts,
        inventoryId,
        orderBackend
    )
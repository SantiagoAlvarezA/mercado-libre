package com.mercadolibre.data.mappers

import com.mercadolibre.data.server.model.CategoryResponse
import com.mercadolibre.domain.model.Category
import com.mercadolibre.domain.model.Picture
import com.mercadolibre.data.database.model.Picture as DomainPicture
import com.mercadolibre.data.server.model.Picture as ServerPicture

fun Picture.toRoomPicture(itemId: String): DomainPicture = DomainPicture(
    id,
    itemId,
    url,
    secureUrl
)

fun DomainPicture.toDomainItem(): Picture = Picture(
    id,
    itemId,
    url,
    secureUrl
)

fun ServerPicture.toDomainPicture(itemId: String): Picture = Picture(
    id,
    itemId,
    url,
    secureUrl
)

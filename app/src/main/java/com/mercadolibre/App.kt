package com.mercadolibre

import androidx.multidex.MultiDexApplication
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class App : MultiDexApplication() {
    companion object {
        private var date: String? = ""

        fun getDate(): String = date ?: ""

        fun setDate(date: String?) {
            this.date = date
        }
    }

    override fun onCreate() {
        super.onCreate()
    }
}
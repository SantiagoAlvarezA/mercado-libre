package com.mercadolibre.di.module

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import com.mercadolibre.data.source.local.CategoryLocalDataSource
import com.mercadolibre.data.source.local.LocalDataSource
import com.mercadolibre.data.source.remote.RemoteDataSource
import com.mercadolibre.data.database.dao.CategoryDao
import com.mercadolibre.data.database.dao.ItemDao
import com.mercadolibre.data.database.dao.PictureDao
import com.mercadolibre.data.database.datasource.CategoryDataSource
import com.mercadolibre.data.database.datasource.ItemDataSource
import com.mercadolibre.data.server.api.Api
import com.mercadolibre.data.server.apisource.RemoteDataSourceImpl

@Module
@InstallIn(SingletonComponent::class)
class DataSourceModule {

    @Provides
    fun provideItemDataSource(itemDao: ItemDao, pictureDao: PictureDao): LocalDataSource =
        ItemDataSource(itemDao,pictureDao)

    @Provides
    fun provideCategoryDataSource(categoryDao: CategoryDao): CategoryLocalDataSource =
        CategoryDataSource(categoryDao)

    @Provides
    fun provideRemoteDataSourceImpl(api: Api): RemoteDataSource =
        RemoteDataSourceImpl(api)
}
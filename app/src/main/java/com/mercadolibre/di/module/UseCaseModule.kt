package com.mercadolibre.di.module

import com.mercadolibre.data.repository.Repository
import com.mercadolibre.usecases.UseCase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class UseCaseModule {

    @Provides
    @Singleton
    fun provideUseCase(
        repository: Repository
    ) = UseCase(repository)
}
package com.mercadolibre.di.module

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import com.mercadolibre.data.repository.Repository
import com.mercadolibre.data.source.local.CategoryLocalDataSource
import com.mercadolibre.data.source.local.LocalDataSource
import com.mercadolibre.data.source.remote.RemoteDataSource

@Module
@InstallIn(SingletonComponent::class)
class RepositoryModule {

    @Provides
    fun productRepositoryProvider(
        localDataSource: LocalDataSource,
        categoryLocalDataSource: CategoryLocalDataSource,
        remoteDataSource: RemoteDataSource,
    ) = Repository(
        localDataSource,
        categoryLocalDataSource,
        remoteDataSource
    )
}
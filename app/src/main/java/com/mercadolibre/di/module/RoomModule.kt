package com.mercadolibre.di.module

import android.app.Application
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import com.mercadolibre.data.database.dao.CategoryDao
import com.mercadolibre.data.database.dao.Database
import com.mercadolibre.data.database.dao.ItemDao
import com.mercadolibre.data.database.dao.PictureDao
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class RoomModule {

    @Provides
    @Singleton
    fun provideRoomDatabase(app: Application): Database =
        Room.databaseBuilder(
            app.applicationContext,
            Database::class.java, "mercado_libre_santiago_alvarez.db"
        )
            .allowMainThreadQueries()
            .fallbackToDestructiveMigration()
            .addCallback(object : RoomDatabase.Callback() {
                override fun onCreate(db: SupportSQLiteDatabase) {
                    super.onCreate(db)
                    Thread {
                        //Timber.d("ROOM ===> database")
                    }.start()
                }
            })
            .build()

    @Provides
    @Singleton
    fun provideItemDao(database: Database): ItemDao =
        database.itemDao()

    @Provides
    @Singleton
    fun provideCategoryDao(database: Database): CategoryDao =
        database.categoryDao()


    @Provides
    @Singleton
    fun providePictureDao(database: Database): PictureDao =
        database.pictureDao()
}
package com.mercadolibre.ui.screens.common

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Favorite
import androidx.compose.material.icons.filled.FavoriteBorder
import androidx.compose.material3.Card
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import coil.compose.rememberAsyncImagePainter
import com.mercadolibre.domain.model.Item

@Composable
fun ItemList(item: Item, onItemClick: (Item) -> Unit, favourite: (Item) -> Unit) {
    Card(modifier = Modifier
        .padding(6.dp)
        .height(144.dp)
        .clickable {
            onItemClick(item)
        }) {

        Row(
            modifier = Modifier
                .padding(12.dp)
                .fillMaxSize(),
            verticalAlignment = Alignment.CenterVertically
        ) {
            Image(
                painter = rememberAsyncImagePainter(item.thumbnail),
                contentDescription = item.title ?: "",
                contentScale = ContentScale.Crop,
                modifier = Modifier
                    .height(100.dp)
                    .width(120.dp)
                    .background(Color.Transparent)
                    .aspectRatio(1f)
                    .clip(RoundedCornerShape(12.dp))
            )

            Column(modifier = Modifier.padding(start = 12.dp)) {
                Text(
                    text = item.title ?: "",
                    maxLines = 3,
                    overflow = TextOverflow.Ellipsis,
                    fontWeight = FontWeight.Light
                )
                Spacer(modifier = Modifier.height(12.dp))

                Row(
                    verticalAlignment = Alignment.CenterVertically,
                    horizontalArrangement = Arrangement.SpaceEvenly,
                    modifier = Modifier.fillMaxWidth()
                ) {

                    Text(
                        text = "$ ${item.price}",
                        maxLines = 1,
                        overflow = TextOverflow.Ellipsis,
                        fontWeight = FontWeight.Bold
                    )
                    Spacer(modifier = Modifier.width(12.dp))
                    IconButton(
                        onClick = { favourite(item) }) {
                        Icon(
                            imageVector = if (item.favourite) Icons.Filled.Favorite else Icons.Filled.FavoriteBorder,
                            contentDescription = ""
                        )
                    }
                }

            }


        }


    }

}
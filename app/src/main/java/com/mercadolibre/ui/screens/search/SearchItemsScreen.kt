package com.mercadolibre.ui.screens.search

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.items
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.mercadolibre.domain.model.Item
import com.mercadolibre.ui.screens.common.EmptyState
import com.mercadolibre.ui.screens.common.ItemList
import com.mercadolibre.ui.screens.common.SearchAppBar


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun SearchItemsScreen(
    category: String? = null,
    toBack: () -> Unit,
    onItemClick: (Item) -> Unit,
    viewModel: SearchItemsViewModel = hiltViewModel(),
) {
    val query = rememberSaveable {
        mutableStateOf("")
    }
    val items: List<Item> by viewModel.items.observeAsState(emptyList())

    Scaffold(
        topBar = {
            TopAppBar(
                title = {
                    if (category.isNullOrEmpty()) {
                        SearchAppBar {
                            if (it.isNotEmpty())
                                viewModel.search(it)
                        }
                    } else {
                        Text(text = category)
                        viewModel.search(category)
                    }
                },
                navigationIcon = {
                    IconButton(onClick = {
                        toBack()
                    }) {
                        Icon(
                            imageVector = Icons.Filled.ArrowBack,
                            contentDescription = "Back"
                        )
                    }
                },
            )
        }

    ) { contentPadding ->
        Box(modifier = Modifier.padding(contentPadding)) {
            if (items.isEmpty()) {
                EmptyState {}
            } else
                Column(
                    modifier = Modifier
                        .fillMaxSize()
                ) {
                    LazyVerticalGrid(
                        columns = GridCells.Adaptive(180.dp),
                        contentPadding = PaddingValues(4.dp)
                    ) {
                        items(items) { item ->
                            if (!item.favourite)
                                ItemList(
                                    item = item,
                                    onItemClick = {
                                        onItemClick(it)
                                    }, favourite = {
                                        it.favourite = !it.favourite
                                        viewModel.update(it)
                                    })
                        }
                    }
                }
        }
    }
}
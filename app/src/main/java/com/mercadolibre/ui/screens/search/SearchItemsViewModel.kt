package com.mercadolibre.ui.screens.search

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.mercadolibre.domain.model.Item
import com.mercadolibre.usecases.UseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SearchItemsViewModel @Inject constructor(
    private val useCase: UseCase
) : ViewModel() {

    private val _items = useCase.getAllItems().asLiveData(Dispatchers.IO)
    val items: LiveData<List<Item>>
        get() = _items



    fun search(search: String) = viewModelScope.launch {
        val result = useCase.searchItems(search)
    }

    fun update(item: Item) = viewModelScope.launch {
        useCase.changeFavourite(item.id, item.favourite)
    }
}
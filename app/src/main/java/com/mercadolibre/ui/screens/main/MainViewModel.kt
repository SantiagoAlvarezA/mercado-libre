package com.mercadolibre.ui.screens.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.mercadolibre.domain.model.Category
import com.mercadolibre.domain.model.Item
import com.mercadolibre.usecases.UseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val useCase: UseCase
) : ViewModel() {

    private val _favourites = useCase.getAllFavourites().asLiveData(Dispatchers.IO)
    val favourites: LiveData<List<Item>>
        get() = _favourites

    private val _items = useCase.getAllItems().asLiveData(Dispatchers.IO)
    val items: LiveData<List<Item>>
        get() = _items

    private val _categories = useCase.getAllCategories().asLiveData(Dispatchers.IO)
    val categories: LiveData<List<Category>>
        get() = _categories

    init {
        viewModelScope.launch {
            useCase.invoke()
        }
    }

    fun search(search: String) = viewModelScope.launch {
        val result = useCase.searchItems(search)
    }

    fun update(item: Item) = viewModelScope.launch{
        useCase.changeFavourite(item.id,item.favourite)
    }

}
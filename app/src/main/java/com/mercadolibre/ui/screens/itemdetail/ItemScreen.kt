package com.mercadolibre.ui.screens.itemdetail

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.items
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.Favorite
import androidx.compose.material.icons.filled.FavoriteBorder
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import coil.compose.rememberAsyncImagePainter
import com.mercadolibre.domain.model.Item
import com.mercadolibre.domain.model.Picture
import com.mercadolibre.ui.screens.common.ItemList

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ItemScreen(
    id: String,
    viewModel: ItemViewModel = hiltViewModel(),
    toBack: () -> Unit
) {

    val item: Item by viewModel.item.observeAsState(Item("0"))
    val pictures: List<Picture> by viewModel.pictures.observeAsState(emptyList())
    val loading: Boolean by viewModel.loading.collectAsState()

    viewModel.getItem(id)
    viewModel.getPictures(id)
    viewModel.getAllPictures(id)


    Scaffold(
        topBar = {
            TopAppBar(
                title = {
                    Text(
                        "Producto",
                        maxLines = 1,
                        overflow = TextOverflow.Ellipsis
                    )
                },
                navigationIcon = {
                    IconButton(onClick = {
                        toBack()
                    }) {
                        Icon(
                            imageVector = Icons.Filled.ArrowBack,
                            contentDescription = "Back"
                        )
                    }
                },
                actions = {

                    IconButton(onClick = {
                        viewModel.changeFavourite(item)
                    }) {
                        Icon(
                            imageVector =
                            if (item.favourite)
                                Icons.Filled.Favorite
                            else
                                Icons.Filled.FavoriteBorder,
                            contentDescription = "Favourite"
                        )
                    }
                },
            )
        }

    ) { contentPadding ->
        Box(modifier = Modifier.padding(contentPadding)) {


            Column {

                if (loading)
                    LinearProgressIndicator(
                        modifier = Modifier
                            .fillMaxWidth()
                            .semantics(mergeDescendants = true) {}
                            .padding(10.dp)
                    )
                LazyVerticalGrid(
                    modifier = Modifier
                        .padding(12.dp),
                    columns = GridCells.Adaptive(380.dp),
                    contentPadding = PaddingValues(6.dp)
                ) {
                    item {
                        Row(
                            Modifier
                                .fillMaxWidth()
                                .padding(12.dp)
                        ) {
                            Image(
                                painter = rememberAsyncImagePainter(item.thumbnail),
                                contentDescription = item.title ?: "",
                                contentScale = ContentScale.Crop,
                                modifier = Modifier
                                    .height(80.dp)
                                    .width(80.dp)
                                    .background(Color.Transparent)
                                    .aspectRatio(1f)
                                    .clip(CircleShape)
                            )
                            Column(modifier = Modifier.padding(start = 12.dp)) {
                                Text(text = item.title ?: "")
                                Spacer(modifier = Modifier.height(12.dp))
                                Text(
                                    text = "$ ${item.price}",
                                    maxLines = 1,
                                    overflow = TextOverflow.Ellipsis,
                                    fontWeight = FontWeight.Bold
                                )

                            }
                        }
                    }

                    items(pictures) { picture ->
                        Image(
                            painter = rememberAsyncImagePainter(picture.url),
                            contentDescription = item.title ?: "",
                            contentScale = ContentScale.Crop,
                            modifier = Modifier
                                .padding(vertical = 4.dp)
                                .background(Color.Transparent)
                                .aspectRatio(1f)
                                .clip(RoundedCornerShape(12.dp))
                        )
                    }
                }
            }

        }
    }
}
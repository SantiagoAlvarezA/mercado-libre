package com.mercadolibre.ui.screens.common

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Clear
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType


@OptIn(ExperimentalMaterial3Api::class, ExperimentalComposeUiApi::class)
@Composable
fun SearchAppBar(
    query: (String) -> Unit
) {
    val query = rememberSaveable { mutableStateOf("") }
    val showClearIcon = rememberSaveable { mutableStateOf(false) }
    val keyboardController = LocalSoftwareKeyboardController.current

    val focusRequester = remember {
        FocusRequester()
    }

    if (query.value.isEmpty()) {
        showClearIcon.value = false
    } else if (query.value.isNotEmpty()) {
        showClearIcon.value = true
    }

    TextField(
        value = query.value,
        onValueChange = { onQueryChanged ->
            // If user makes changes to text, immediately updated it.
            query.value = onQueryChanged
            // To avoid crash, only query when string isn't empty.
            if (onQueryChanged.isNotEmpty()) {
                // Pass latest query to refresh search results.
            }
           // query(query.value)
        },
        trailingIcon = {
            if (showClearIcon.value) {
                IconButton(onClick = { query.value = "" }) {
                    Icon(
                        imageVector = Icons.Rounded.Clear,
                        tint = MaterialTheme.colorScheme.onBackground,
                        contentDescription = "Clear Icon"
                    )
                }
            }
        },
        maxLines = 1,
        colors = TextFieldDefaults.textFieldColors(),
        placeholder = { Text(text = "Buscar") },
        textStyle = MaterialTheme.typography.titleSmall,
        singleLine = true,
        keyboardOptions = KeyboardOptions(
            keyboardType = KeyboardType.Text,
            imeAction = ImeAction.Done
        ),
        keyboardActions = KeyboardActions(
            onDone = {
                query(query.value)
                keyboardController?.hide()
                query.value = ""
                focusRequester.freeFocus()
            }
        ),
        modifier = Modifier
            .fillMaxWidth()
            .focusRequester(focusRequester)
            .background(color = MaterialTheme.colorScheme.background, shape = RectangleShape),
    )
    LaunchedEffect("") {
        focusRequester.requestFocus()
    }
}
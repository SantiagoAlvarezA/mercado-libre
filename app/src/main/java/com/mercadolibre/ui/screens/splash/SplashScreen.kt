package com.mercadolibre.ui.screens.splash

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.mercadolibre.R
import kotlinx.coroutines.delay


@Composable
fun SplashScreen(toMainScreen: () -> Unit) {
    LaunchedEffect(key1 = true) {
        delay(800)
        toMainScreen()
    }
    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(Color(0xfffff159)),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {
        Image(
            painter = painterResource(id = R.drawable.mercadolibre), contentDescription = "Splash"
        )
        Spacer(modifier = Modifier.height(18.dp))
        Text(text = "by Santiago Alvarez", fontSize = 24.sp)
    }
}
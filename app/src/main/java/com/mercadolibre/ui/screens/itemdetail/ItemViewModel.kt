package com.mercadolibre.ui.screens.itemdetail

import androidx.lifecycle.*
import com.mercadolibre.domain.model.Item
import com.mercadolibre.domain.model.Picture
import com.mercadolibre.domain.resource.Resource
import com.mercadolibre.usecases.UseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class ItemViewModel @Inject constructor(
    private val useCase: UseCase
) : ViewModel() {

    private var _item = MutableLiveData<Item>()
    val item: LiveData<Item>
        get() = _item

    private var _pictures = MutableLiveData<List<Picture>>()
    val pictures: LiveData<List<Picture>>
        get() = _pictures

    private val _loading = MutableStateFlow(true)
    val loading: StateFlow<Boolean> get() = _loading


    fun getItem(id: String) = viewModelScope.launch {
        val item = withContext(Dispatchers.IO) {
            useCase.getItem(id)
        }
        item.collectLatest {
            _item.value = it
        }

    }

    fun changeFavourite(item: Item) = viewModelScope.launch {
        useCase.changeFavourite(item.id, !item.favourite)
    }

    fun getPictures(itemId: String) = viewModelScope.launch {

        when (val result = useCase.getItemPictures(itemId)) {
            is Resource.Success -> {
                _loading.value = false
            }
            else -> _loading.value = false
        }

    }

    fun getAllPictures(itemId: String) = viewModelScope.launch {
        val pictures = withContext(Dispatchers.IO) {
            useCase.getAllPictures(itemId)
        }
        pictures.collectLatest {
            _pictures.value = it
        }
    }
}
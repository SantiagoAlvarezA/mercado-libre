package com.mercadolibre.ui.screens.main

import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.items
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Menu
import androidx.compose.material.icons.filled.Search
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import com.mercadolibre.domain.model.Category
import com.mercadolibre.domain.model.Item
import com.mercadolibre.ui.screens.common.EmptyState
import com.mercadolibre.ui.screens.common.ItemList
import kotlinx.coroutines.launch


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun MainScreen(
    viewModel: MainViewModel = hiltViewModel(),
    onItemClick: (Item) -> Unit,
    onSearch: () -> Unit,
    onCategory: (String) -> Unit
) {
    val categories: List<Category> by viewModel.categories.observeAsState(emptyList())
    val items: List<Item> by viewModel.items.observeAsState(emptyList())
    val favourites: List<Item> by viewModel.favourites.observeAsState(emptyList())

    val scrollBehavior = TopAppBarDefaults.pinnedScrollBehavior()
    val drawerState = rememberDrawerState(DrawerValue.Closed)
    val scope = rememberCoroutineScope()


    val selectedItem = remember { mutableStateOf(Category("", "Mercado Libre")) }
    ModalNavigationDrawer(
        drawerState = drawerState,
        drawerContent = {
            ModalDrawerSheet(
                modifier = Modifier.verticalScroll(rememberScrollState()),
            ) {
                Spacer(Modifier.height(12.dp))
                Text(
                    text = "Categories",
                    modifier = Modifier.padding(start = 12.dp),
                    fontSize = 24.sp,
                    overflow = TextOverflow.Ellipsis,
                    fontWeight = FontWeight.Bold
                )
                Spacer(Modifier.height(12.dp))

                categories.forEach { item ->
                    NavigationDrawerItem(
                        //icon = { Icon(item, contentDescription = null) },
                        label = { Text(item.name) },
                        selected = item == selectedItem.value,
                        onClick = {
                            scope.launch {
                                drawerState.close()
                                onCategory(item.name)
                            }
                            selectedItem.value = item

                        },
                        modifier = Modifier.padding(NavigationDrawerItemDefaults.ItemPadding)
                    )
                }
            }
        },
        content = {
            Scaffold(
                topBar = {
                    TopAppBar(
                        title = {


                            Text(
                                "Mercado Libre",
                                maxLines = 1,
                                overflow = TextOverflow.Ellipsis
                            )
                        },
                        navigationIcon = {
                            IconButton(onClick = {
                                scope.launch {
                                    drawerState.open()
                                }
                            }) {
                                Icon(
                                    imageVector = Icons.Filled.Menu,
                                    contentDescription = "Menu"
                                )
                            }
                        },
                        actions = {
                            IconButton(onClick = {
                                onSearch()
                            }) {
                                Icon(
                                    imageVector = Icons.Filled.Search,
                                    contentDescription = "Buscar"
                                )
                            }
                        },
                        scrollBehavior = scrollBehavior
                    )
                }

            ) { contentPadding ->
                Box(modifier = Modifier.padding(contentPadding)) {
                    if (items.isEmpty()) {
                        EmptyState {
                            onSearch()
                        }
                    } else
                        Column(
                            modifier = Modifier
                                .fillMaxSize()
                        ) {
                            LazyVerticalGrid(
                                columns = GridCells.Adaptive(380.dp),
                                contentPadding = PaddingValues(4.dp)
                            ) {
                                item {
                                    if (favourites.isNotEmpty())
                                        Text(
                                            text = "Favourites",
                                            fontSize = 24.sp,
                                            fontWeight = FontWeight.Bold
                                        )
                                }
                                items(favourites) { favourite ->
                                    ItemList(item = favourite, onItemClick = {
                                        onItemClick(favourite)
                                    }, favourite = {
                                        it.favourite = !it.favourite
                                        viewModel.update(it)
                                    })
                                }
                                item {
                                    if (items.isNotEmpty())
                                        Text(
                                            text = "Last search",
                                            fontSize = 24.sp,
                                            fontWeight = FontWeight.Bold
                                        )
                                }

                                items(items) { item ->
                                    if (!item.favourite)
                                        ItemList(item = item, onItemClick = {
                                            onItemClick(it)
                                        }, favourite = {
                                            it.favourite = !it.favourite
                                            viewModel.update(it)
                                        })
                                }
                            }
                        }
                }
            }
        }
    )
}


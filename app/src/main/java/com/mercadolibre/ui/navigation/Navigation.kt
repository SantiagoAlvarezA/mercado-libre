package com.mercadolibre.ui.navigation

import androidx.compose.runtime.Composable
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.mercadolibre.ui.screens.itemdetail.ItemScreen
import com.mercadolibre.ui.screens.main.MainScreen
import com.mercadolibre.ui.screens.search.SearchItemsScreen
import com.mercadolibre.ui.screens.splash.SplashScreen

@Composable
fun Navigation() {
    val navController = rememberNavController()
    NavHost(navController = navController, startDestination = Screen.SplashScreen.route) {

        composable(route = Screen.SplashScreen.route) {
            SplashScreen {
                navController.popBackStack()
                navController.navigate(Screen.MainScreen.route)
            }
        }
        composable(route = Screen.MainScreen.route) {
            MainScreen(
                onItemClick = { _it ->
                    navController.navigate(Screen.ItemScreen.withArgs(_it.id))
                },
                onSearch = {
                    navController.navigate(Screen.SearchItemsScreen.route)
                },
                onCategory = {
                    navController.navigate("${Screen.SearchItemsScreen.route}?category=$it")
                })
        }
        composable(
            route = "${Screen.ItemScreen.route}/{id}",
            arguments = listOf(navArgument("id") { type = NavType.StringType })
        ) {
            val id = it.arguments?.getString("id")
            requireNotNull(id)
            ItemScreen(id) {
                navController.popBackStack()
            }
        }
        composable(
            route = "${Screen.SearchItemsScreen.route}?category={category}",
            arguments = listOf(navArgument("category") {
                type = NavType.StringType
                nullable = true
            })
        ) {
            val category = it.arguments?.getString("category")
            SearchItemsScreen(
                category = category,
                toBack = {
                    navController.popBackStack()
                },
                onItemClick = { _it ->
                    navController.popBackStack()
                    navController.navigate(Screen.ItemScreen.withArgs(_it.id))
                }
            )
        }

    }
}
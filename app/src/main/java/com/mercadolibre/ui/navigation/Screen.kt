package com.mercadolibre.ui.navigation

sealed class Screen(val route: String) {
    object SplashScreen : Screen("splash")
    object MainScreen : Screen("main")
    object ItemScreen : Screen("item")
    object SearchItemsScreen : Screen("search")

    fun withArgs(vararg args: String): String =
        buildString {
            append(route)
            args.forEach { arg ->
                append("/$arg")
            }
        }
}